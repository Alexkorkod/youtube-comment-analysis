package model

import "time"

type ChannelStatistics struct {
	Id                   int       `json:"id"`
	ChannelId            string    `json:"channel_id"`
	ChannelName          string    `json:"channel_name"`
	AverageLength        float64   `json:"avg_length"`
	MedianLength         float64   `json:"medial_length"`
	AverageProfanityRate float64   `json:"avg_prof_rate"`
	AverageUserRate      float64   `json:"avg_user_rate"`
	AverageMistakeAmount float64   `json:"avg_mistakes"`
	LastProcessed        time.Time `json:"last_processed"`
}

type UserRating struct {
	StatisticsID int       `json:"statistics_id"`
	RatingTime   time.Time `json:"rating_time"`
	Rating       int       `json:"rating"`
}

type StatisticsBacklog struct {
	ChannelId     string     `json:"channel_id"`
	ChannelName   string     `json:"channel_name"`
	LastRequested time.Time  `json:"last_requested"`
	LastProcessed *time.Time `json:"last_processed"`
}
