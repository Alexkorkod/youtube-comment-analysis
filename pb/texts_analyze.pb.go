// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.25.0
// 	protoc        v3.15.1
// source: texts_analyze.proto

package pb

import (
	proto "github.com/golang/protobuf/proto"
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// This is a compile-time assertion that a sufficiently up-to-date version
// of the legacy proto package is being used.
const _ = proto.ProtoPackageIsVersion4

type AnalyzeTextRequest struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Comments []string `protobuf:"bytes,1,rep,name=comments,proto3" json:"comments,omitempty"`
}

func (x *AnalyzeTextRequest) Reset() {
	*x = AnalyzeTextRequest{}
	if protoimpl.UnsafeEnabled {
		mi := &file_texts_analyze_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AnalyzeTextRequest) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AnalyzeTextRequest) ProtoMessage() {}

func (x *AnalyzeTextRequest) ProtoReflect() protoreflect.Message {
	mi := &file_texts_analyze_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AnalyzeTextRequest.ProtoReflect.Descriptor instead.
func (*AnalyzeTextRequest) Descriptor() ([]byte, []int) {
	return file_texts_analyze_proto_rawDescGZIP(), []int{0}
}

func (x *AnalyzeTextRequest) GetComments() []string {
	if x != nil {
		return x.Comments
	}
	return nil
}

type AnalyzeTextResponse struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Rates    []float64 `protobuf:"fixed64,1,rep,packed,name=rates,proto3" json:"rates,omitempty"`
	Mistakes []int32   `protobuf:"varint,2,rep,packed,name=mistakes,proto3" json:"mistakes,omitempty"`
}

func (x *AnalyzeTextResponse) Reset() {
	*x = AnalyzeTextResponse{}
	if protoimpl.UnsafeEnabled {
		mi := &file_texts_analyze_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *AnalyzeTextResponse) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*AnalyzeTextResponse) ProtoMessage() {}

func (x *AnalyzeTextResponse) ProtoReflect() protoreflect.Message {
	mi := &file_texts_analyze_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use AnalyzeTextResponse.ProtoReflect.Descriptor instead.
func (*AnalyzeTextResponse) Descriptor() ([]byte, []int) {
	return file_texts_analyze_proto_rawDescGZIP(), []int{1}
}

func (x *AnalyzeTextResponse) GetRates() []float64 {
	if x != nil {
		return x.Rates
	}
	return nil
}

func (x *AnalyzeTextResponse) GetMistakes() []int32 {
	if x != nil {
		return x.Mistakes
	}
	return nil
}

var File_texts_analyze_proto protoreflect.FileDescriptor

var file_texts_analyze_proto_rawDesc = []byte{
	0x0a, 0x13, 0x74, 0x65, 0x78, 0x74, 0x73, 0x5f, 0x61, 0x6e, 0x61, 0x6c, 0x79, 0x7a, 0x65, 0x2e,
	0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x02, 0x70, 0x62, 0x22, 0x30, 0x0a, 0x12, 0x41, 0x6e, 0x61,
	0x6c, 0x79, 0x7a, 0x65, 0x54, 0x65, 0x78, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x12,
	0x1a, 0x0a, 0x08, 0x63, 0x6f, 0x6d, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28,
	0x09, 0x52, 0x08, 0x63, 0x6f, 0x6d, 0x6d, 0x65, 0x6e, 0x74, 0x73, 0x22, 0x47, 0x0a, 0x13, 0x41,
	0x6e, 0x61, 0x6c, 0x79, 0x7a, 0x65, 0x54, 0x65, 0x78, 0x74, 0x52, 0x65, 0x73, 0x70, 0x6f, 0x6e,
	0x73, 0x65, 0x12, 0x14, 0x0a, 0x05, 0x72, 0x61, 0x74, 0x65, 0x73, 0x18, 0x01, 0x20, 0x03, 0x28,
	0x01, 0x52, 0x05, 0x72, 0x61, 0x74, 0x65, 0x73, 0x12, 0x1a, 0x0a, 0x08, 0x6d, 0x69, 0x73, 0x74,
	0x61, 0x6b, 0x65, 0x73, 0x18, 0x02, 0x20, 0x03, 0x28, 0x05, 0x52, 0x08, 0x6d, 0x69, 0x73, 0x74,
	0x61, 0x6b, 0x65, 0x73, 0x32, 0x50, 0x0a, 0x0c, 0x54, 0x65, 0x78, 0x74, 0x41, 0x6e, 0x61, 0x6c,
	0x79, 0x7a, 0x65, 0x72, 0x12, 0x40, 0x0a, 0x0b, 0x41, 0x6e, 0x61, 0x6c, 0x79, 0x7a, 0x65, 0x54,
	0x65, 0x78, 0x74, 0x12, 0x16, 0x2e, 0x70, 0x62, 0x2e, 0x41, 0x6e, 0x61, 0x6c, 0x79, 0x7a, 0x65,
	0x54, 0x65, 0x78, 0x74, 0x52, 0x65, 0x71, 0x75, 0x65, 0x73, 0x74, 0x1a, 0x17, 0x2e, 0x70, 0x62,
	0x2e, 0x41, 0x6e, 0x61, 0x6c, 0x79, 0x7a, 0x65, 0x54, 0x65, 0x78, 0x74, 0x52, 0x65, 0x73, 0x70,
	0x6f, 0x6e, 0x73, 0x65, 0x22, 0x00, 0x42, 0x33, 0x5a, 0x31, 0x67, 0x69, 0x74, 0x6c, 0x61, 0x62,
	0x2e, 0x63, 0x6f, 0x6d, 0x2f, 0x61, 0x6c, 0x65, 0x78, 0x6b, 0x6f, 0x72, 0x6b, 0x6f, 0x64, 0x2f,
	0x79, 0x6f, 0x75, 0x74, 0x75, 0x62, 0x65, 0x2d, 0x63, 0x6f, 0x6d, 0x6d, 0x65, 0x6e, 0x74, 0x2d,
	0x61, 0x6e, 0x61, 0x6c, 0x79, 0x73, 0x69, 0x73, 0x2f, 0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f,
	0x74, 0x6f, 0x33,
}

var (
	file_texts_analyze_proto_rawDescOnce sync.Once
	file_texts_analyze_proto_rawDescData = file_texts_analyze_proto_rawDesc
)

func file_texts_analyze_proto_rawDescGZIP() []byte {
	file_texts_analyze_proto_rawDescOnce.Do(func() {
		file_texts_analyze_proto_rawDescData = protoimpl.X.CompressGZIP(file_texts_analyze_proto_rawDescData)
	})
	return file_texts_analyze_proto_rawDescData
}

var file_texts_analyze_proto_msgTypes = make([]protoimpl.MessageInfo, 2)
var file_texts_analyze_proto_goTypes = []interface{}{
	(*AnalyzeTextRequest)(nil),  // 0: pb.AnalyzeTextRequest
	(*AnalyzeTextResponse)(nil), // 1: pb.AnalyzeTextResponse
}
var file_texts_analyze_proto_depIdxs = []int32{
	0, // 0: pb.TextAnalyzer.AnalyzeText:input_type -> pb.AnalyzeTextRequest
	1, // 1: pb.TextAnalyzer.AnalyzeText:output_type -> pb.AnalyzeTextResponse
	1, // [1:2] is the sub-list for method output_type
	0, // [0:1] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_texts_analyze_proto_init() }
func file_texts_analyze_proto_init() {
	if File_texts_analyze_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_texts_analyze_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AnalyzeTextRequest); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_texts_analyze_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*AnalyzeTextResponse); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_texts_analyze_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   2,
			NumExtensions: 0,
			NumServices:   1,
		},
		GoTypes:           file_texts_analyze_proto_goTypes,
		DependencyIndexes: file_texts_analyze_proto_depIdxs,
		MessageInfos:      file_texts_analyze_proto_msgTypes,
	}.Build()
	File_texts_analyze_proto = out.File
	file_texts_analyze_proto_rawDesc = nil
	file_texts_analyze_proto_goTypes = nil
	file_texts_analyze_proto_depIdxs = nil
}
