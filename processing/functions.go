package processing

import (
	"context"
	"errors"
	"gitlab.com/alexkorkod/youtube-comment-analysis/model"
	"gitlab.com/alexkorkod/youtube-comment-analysis/pb"
	"google.golang.org/grpc"
	"log"
	"sort"
	"time"

	"google.golang.org/api/option"
	"google.golang.org/api/youtube/v3"

	"github.com/jackc/pgx"
)

var DeveloperKey string
var DbConn *pgx.Conn

//сохраняем статистические данные по комментариям
func GetAllHeadCommentsData(filterString string, filterType string) (result model.ChannelStatistics, err error) {
	var channelId string
	ctx := context.Background()
	service, err := youtube.NewService(ctx, option.WithAPIKey(DeveloperKey))
	if err != nil {
		log.Printf("Error creating new YouTube client: %v\n", err)
		return
	}

	if filterType == "name" {
		call := service.Channels.List([]string{"snippet", "contentDetails", "statistics"}).
			ForUsername(filterString)

		response, err := call.Do()
		if err != nil {
			return result, err
		}

		if len(response.Items) == 0 {
			return result, errors.New("no such channel found")
		}
		//will need Snippet.defaultlanguage and title and (Statistics.Subscribers.count)?
		channelId = response.Items[0].Id
	} else {
		channelId = filterString
	}
	nextPageToken := ""
	allCommentLengthList := make([]int, 0)
	allRates := make([]float64, 0)
	allMistakes := make([]int, 0)
	totalLength := 0

	//look at no more then 5000 comment threads
	maxDepth := 50
	curDepth := 0
	log.Printf("starting comment lookup")
	for nextPageExists := true; nextPageExists; nextPageExists = nextPageToken != "" {
		thisPageCommentLengths := make([]int, 0)
		thisPageProfanityRates := make([]float64, 0)
		var thisPageMistakes, thisPageLength int

		thisPageCommentLengths, thisPageProfanityRates, thisPageMistakes, thisPageLength, err = getOnePageOfStats(
			channelId,
			&curDepth,
			&nextPageToken,
			service,
		)

		if err != nil {
			return result, err
		}
		allCommentLengthList = append(allCommentLengthList, thisPageCommentLengths...)
		allRates = append(allRates, thisPageProfanityRates...)
		allMistakes = append(allMistakes, thisPageMistakes)
		totalLength = totalLength + thisPageLength

		if curDepth >= maxDepth {
			break
		}
	}

	var averageCommentLength float64 = 0
	var medianCommentLength float64 = 0
	var averageMistakesAmount float64 = 0
	if len(allCommentLengthList) > 0 {
		averageCommentLength = GetAvg(allCommentLengthList)
		medianCommentLength = findMedian(allCommentLengthList)
		averageMistakesAmount = GetAvgWithLength(allMistakes, totalLength)
	}
	avgRate := getAvgRate(allRates)

	var recordId int
	sqlString := "SELECT id FROM channel_statistics WHERE channel_id = $1"
	searchErr := DbConn.QueryRow(sqlString, channelId).Scan(&recordId)
	if searchErr != nil && searchErr != pgx.ErrNoRows {
		return
	}

	timestampLayout := "2006-01-02 15:04:05-07"
	result.LastProcessed = time.Now()
	if recordId == 0 {
		log.Printf("creating new statistics record")
		if filterType == "id" {
			filterString = ""
		}
		sqlString = "INSERT INTO channel_statistics(channel_id, comment_len_avg, comment_len_median, comment_profanity_rate_avg, channel_name, mistakes_amount_avg, last_processed) " +
			"VALUES($1, $2, $3, $4, $5, $6, &7)"
		_, err = DbConn.Exec(sqlString, channelId, averageCommentLength, medianCommentLength, avgRate, filterString, averageMistakesAmount, result.LastProcessed.Format(timestampLayout))
		if err != nil {
			return
		}
	} else {
		log.Printf("updating existing statistics record")
		sqlString = "UPDATE channel_statistics SET comment_len_avg = $1, comment_len_median = $2, comment_profanity_rate_avg = $3, mistakes_amount_avg = $4, last_processed = $5 " +
			"WHERE id = $6"
		_, err = DbConn.Exec(sqlString, averageCommentLength, medianCommentLength, avgRate, averageMistakesAmount, result.LastProcessed.Format(timestampLayout), recordId)
		if err != nil {
			return
		}
	}

	result = model.ChannelStatistics{
		AverageLength:        averageCommentLength,
		MedianLength:         medianCommentLength,
		AverageProfanityRate: avgRate,
		AverageMistakeAmount: averageMistakesAmount,
	}

	return result, nil
}

func getOnePageOfStats(channelId string, curDepth *int, nextPageToken *string, service *youtube.Service) ([]int, []float64, int, int, error) {
	*curDepth = *curDepth + 1
	commentThreadsCall := service.CommentThreads.List([]string{"snippet", "replies"}).
		AllThreadsRelatedToChannelId(channelId).
		Order("time").
		MaxResults(100)

	if *nextPageToken != "" {
		commentThreadsCall = commentThreadsCall.PageToken(*nextPageToken)
	}
	threadResponse, err := commentThreadsCall.Do()
	if err != nil {
		return make([]int, 0), make([]float64, 0), 0, 0, err
	}

	*nextPageToken = threadResponse.NextPageToken
	summaryCommentsLength := 0
	commentLengthsSlice := make([]int, 0)

	thisSliceComments := make([]string, 0)
	for _, thread := range threadResponse.Items {
		thisCommentContent := thread.Snippet.TopLevelComment.Snippet.TextOriginal
		thisSliceComments = append(thisSliceComments, thisCommentContent)
		thisCommentContentLength := len(thisCommentContent)
		commentLengthsSlice = append(commentLengthsSlice, thisCommentContentLength)
		summaryCommentsLength = summaryCommentsLength + thisCommentContentLength
	}

	profanityRates, grammarMistakes := getProfanityAndGrammarMetrics(thisSliceComments)

	return commentLengthsSlice, profanityRates, grammarMistakes, len(thisSliceComments), nil
}

//slow (n*log(n)) but simple
//rewrite to 'quickselect'(~n) later for bg data
func findMedian(allInts []int) (median float64) {
	sort.Ints(allInts)
	if len(allInts)%2 == 1 {
		return float64(allInts[len(allInts)/2])
	} else {
		return 0.5 * float64(allInts[len(allInts)/2-1]+allInts[len(allInts)/2])
	}
}

func GetAvg(iList []int) float64 {
	if len(iList) == 0 {
		return 0
	}

	sum := 0
	for _, i := range iList {
		sum += i
	}

	//это легальное приведение? найти best practice
	return float64(sum) / float64(len(iList))
}

func GetAvgWithLength(iList []int, length int) float64 {
	if len(iList) == 0 {
		return 0
	}

	sum := 0
	for _, i := range iList {
		sum += i
	}

	//это легальное приведение? найти best practice
	return float64(sum) / float64(length)
}

func getAvgRate(rates []float64) float64 {
	if len(rates) == 0 {
		return 0.0
	}

	sum := 0.0
	for _, rate := range rates {
		sum += rate
	}

	return sum / float64(len(rates))
}

func getProfanityAndGrammarMetrics(texts []string) ([]float64, int) {
	profRates := make([]float64, 0)
	grammarRate := 0

	addr := "localhost:9999"
	conn, err := grpc.Dial(addr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		log.Println(err)
		return profRates, grammarRate
	}
	defer conn.Close()

	client := pb.NewTextAnalyzerClient(conn)
	req := pb.AnalyzeTextRequest{
		Comments: texts,
	}

	analyzerResponse, err := client.AnalyzeText(context.Background(), &req)
	if err != nil {
		log.Println(err)
		return profRates, grammarRate
	}
	profRates = analyzerResponse.Rates
	if len(analyzerResponse.Mistakes) > 0 {
		grammarRate = int(analyzerResponse.Mistakes[0])
	}

	return analyzerResponse.Rates, grammarRate
}
