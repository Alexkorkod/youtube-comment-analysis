import logging
from concurrent.futures import ThreadPoolExecutor

import grpc
from language_tool_python import LanguageTool
from profanity_check import predict_prob

from texts_analyze_pb2 import AnalyzeTextResponse
from texts_analyze_pb2_grpc import TextAnalyzerServicer, add_TextAnalyzerServicer_to_server


class TextAnalyzerServer(TextAnalyzerServicer):
    def __init__(self):
        self.tool = LanguageTool('en-US')

    def AnalyzeText(self, request, context):
        logging.info('rating profanity, request size: %d', len(request.comments))
        # Convert metrics to numpy array of values only

        rates = predict_prob(request.comments)

        mistakes = []
        all_comment_string = ' '.join(request.comments)
        mistakes.append(len(self.tool.check(all_comment_string)))

        resp = AnalyzeTextResponse(rates=rates, mistakes=mistakes)
        return resp


if __name__ == '__main__':
    logging.basicConfig(
        level=logging.INFO,
        format='%(asctime)s - %(levelname)s - %(message)s',
    )
server = grpc.server(ThreadPoolExecutor())
add_TextAnalyzerServicer_to_server(TextAnalyzerServer(), server)
port = 9999
server.add_insecure_port(f'[::]:{port}')
server.start()
logging.info('server ready on port %r', port)
server.wait_for_termination()
