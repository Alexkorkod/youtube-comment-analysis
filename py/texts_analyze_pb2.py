# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: texts_analyze.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='texts_analyze.proto',
  package='pb',
  syntax='proto3',
  serialized_options=b'Z1gitlab.com/alexkorkod/youtube-comment-analysis/pb',
  create_key=_descriptor._internal_create_key,
  serialized_pb=b'\n\x13texts_analyze.proto\x12\x02pb\"&\n\x12\x41nalyzeTextRequest\x12\x10\n\x08\x63omments\x18\x01 \x03(\t\"6\n\x13\x41nalyzeTextResponse\x12\r\n\x05rates\x18\x01 \x03(\x01\x12\x10\n\x08mistakes\x18\x02 \x03(\x05\x32P\n\x0cTextAnalyzer\x12@\n\x0b\x41nalyzeText\x12\x16.pb.AnalyzeTextRequest\x1a\x17.pb.AnalyzeTextResponse\"\x00\x42\x33Z1gitlab.com/alexkorkod/youtube-comment-analysis/pbb\x06proto3'
)




_ANALYZETEXTREQUEST = _descriptor.Descriptor(
  name='AnalyzeTextRequest',
  full_name='pb.AnalyzeTextRequest',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='comments', full_name='pb.AnalyzeTextRequest.comments', index=0,
      number=1, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=27,
  serialized_end=65,
)


_ANALYZETEXTRESPONSE = _descriptor.Descriptor(
  name='AnalyzeTextResponse',
  full_name='pb.AnalyzeTextResponse',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  create_key=_descriptor._internal_create_key,
  fields=[
    _descriptor.FieldDescriptor(
      name='rates', full_name='pb.AnalyzeTextResponse.rates', index=0,
      number=1, type=1, cpp_type=5, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
    _descriptor.FieldDescriptor(
      name='mistakes', full_name='pb.AnalyzeTextResponse.mistakes', index=1,
      number=2, type=5, cpp_type=1, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR,  create_key=_descriptor._internal_create_key),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto3',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=67,
  serialized_end=121,
)

DESCRIPTOR.message_types_by_name['AnalyzeTextRequest'] = _ANALYZETEXTREQUEST
DESCRIPTOR.message_types_by_name['AnalyzeTextResponse'] = _ANALYZETEXTRESPONSE
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

AnalyzeTextRequest = _reflection.GeneratedProtocolMessageType('AnalyzeTextRequest', (_message.Message,), {
  'DESCRIPTOR' : _ANALYZETEXTREQUEST,
  '__module__' : 'texts_analyze_pb2'
  # @@protoc_insertion_point(class_scope:pb.AnalyzeTextRequest)
  })
_sym_db.RegisterMessage(AnalyzeTextRequest)

AnalyzeTextResponse = _reflection.GeneratedProtocolMessageType('AnalyzeTextResponse', (_message.Message,), {
  'DESCRIPTOR' : _ANALYZETEXTRESPONSE,
  '__module__' : 'texts_analyze_pb2'
  # @@protoc_insertion_point(class_scope:pb.AnalyzeTextResponse)
  })
_sym_db.RegisterMessage(AnalyzeTextResponse)


DESCRIPTOR._options = None

_TEXTANALYZER = _descriptor.ServiceDescriptor(
  name='TextAnalyzer',
  full_name='pb.TextAnalyzer',
  file=DESCRIPTOR,
  index=0,
  serialized_options=None,
  create_key=_descriptor._internal_create_key,
  serialized_start=123,
  serialized_end=203,
  methods=[
  _descriptor.MethodDescriptor(
    name='AnalyzeText',
    full_name='pb.TextAnalyzer.AnalyzeText',
    index=0,
    containing_service=None,
    input_type=_ANALYZETEXTREQUEST,
    output_type=_ANALYZETEXTRESPONSE,
    serialized_options=None,
    create_key=_descriptor._internal_create_key,
  ),
])
_sym_db.RegisterServiceDescriptor(_TEXTANALYZER)

DESCRIPTOR.services_by_name['TextAnalyzer'] = _TEXTANALYZER

# @@protoc_insertion_point(module_scope)
