package api

import (
	"encoding/json"
	"github.com/jackc/pgx"
	"gitlab.com/alexkorkod/youtube-comment-analysis/model"
	"gitlab.com/alexkorkod/youtube-comment-analysis/processing"
	"log"
	"net/http"
)

var DbConn *pgx.Conn

func GetStatsForChannel(w http.ResponseWriter, req *http.Request) {
	ytChannelId := req.URL.Query().Get("channel_id")
	ytChannelName := req.URL.Query().Get("channel_name")
	log.Printf("call to GetStatsForChannel, ch_id = %v, ch_name = %v", ytChannelId, ytChannelName)

	if ytChannelName == "" && ytChannelId == "" {
		http.Error(w, "no param to find channel by", http.StatusBadRequest)
		return
	}

	var result model.ChannelStatistics
	var filterParam string

	sqlString := "SELECT id, channel_id, channel_name, comment_len_avg, comment_len_median, comment_profanity_rate_avg, last_processed " +
		"FROM channel_statistics WHERE "
	if ytChannelId != "" {
		sqlString = sqlString + "channel_id = $1"
		filterParam = ytChannelId
	} else {
		sqlString = sqlString + "channel_name = $1"
		filterParam = ytChannelName
	}

	searchErr := DbConn.QueryRow(sqlString, filterParam).Scan(&result.Id, &result.ChannelId, &result.ChannelName, &result.AverageLength, &result.MedianLength, &result.AverageProfanityRate, &result.LastProcessed)
	if searchErr != nil && searchErr != pgx.ErrNoRows {
		http.Error(w, searchErr.Error(), http.StatusBadRequest)
		return
	}

	userRates := make([]int, 0)
	sqlString = "SELECT rating FROM user_ratings WHERE statistics_id = $1"
	rows, searchErr := DbConn.Query(sqlString, result.Id)
	if searchErr != nil {
		http.Error(w, searchErr.Error(), http.StatusBadRequest)
		return
	}

	for rows.Next() {
		var rate int
		if err := rows.Scan(&rate); err != nil {
			log.Println(err)
			continue
		}

		userRates = append(userRates, rate)
	}

	avgUserRate := processing.GetAvg(userRates)

	result.AverageUserRate = avgUserRate

	log.Printf("call to GetStatsForChannel finished without errors")
	writeJson(w, result)
}

func CalculateStatsForChannel(w http.ResponseWriter, req *http.Request) {
	err := CheckSecret(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	ytChannelId := req.URL.Query().Get("channel_id")
	ytChannelName := req.URL.Query().Get("channel_name")
	log.Printf("call to CalculateStatsForChannel, ch_id = %v, ch_name = %v", ytChannelId, ytChannelName)

	if ytChannelName == "" && ytChannelId == "" {
		http.Error(w, "no param to find channel by", http.StatusBadRequest)
		return
	}

	var filterType string
	if ytChannelName == "" {
		filterType = "id"
	} else {
		filterType = "name"
	}

	//possible bug for requests by channel name TODO check and fix
	result, err := processing.GetAllHeadCommentsData(ytChannelId, filterType)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	log.Printf("call to CalculateStatsForChannel finished without errors")
	writeJson(w, result)
}

func RateChannel(w http.ResponseWriter, req *http.Request) {
	err := CheckSecret(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	ytChannelId := req.URL.Query().Get("channel_id")
	ytChannelName := req.URL.Query().Get("channel_name")
	log.Printf("call to RateChannel, ch_id = %v, ch_name = %v", ytChannelId, ytChannelName)

	if ytChannelName == "" && ytChannelId == "" {
		http.Error(w, "no param to find channel by", http.StatusBadRequest)
		return
	}

	var ratingData model.UserRating

	err = json.NewDecoder(req.Body).Decode(&ratingData)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	var statisticsId int
	var filterParam string
	sqlString := "SELECT id FROM channel_statistics WHERE "
	if ytChannelId != "" {
		sqlString = sqlString + "channel_id = $1"
		filterParam = ytChannelId
	} else {
		sqlString = sqlString + "channel_name = $1"
		filterParam = ytChannelName
	}

	searchErr := DbConn.QueryRow(sqlString, filterParam).Scan(&statisticsId)
	if searchErr != nil {
		http.Error(w, searchErr.Error(), http.StatusBadRequest)
		return
	}

	ratingData.StatisticsID = statisticsId

	sqlString = "INSERT INTO user_ratings(statistics_id, rating_time, rating) " +
		"VALUES($1, $2, $3)"
	_, err = DbConn.Exec(sqlString, ratingData.StatisticsID, ratingData.RatingTime.String(), ratingData.Rating)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	log.Printf("call to RateChannel finished without errors")
	writeJson(w, ratingData)
}
