package api

import (
	"context"
	"github.com/jackc/pgx"
	"gitlab.com/alexkorkod/youtube-comment-analysis/model"
	"gitlab.com/alexkorkod/youtube-comment-analysis/processing"
	"google.golang.org/api/option"
	"google.golang.org/api/youtube/v3"
	"log"
	"net/http"
	"time"
)

func AddChannelToBacklog(w http.ResponseWriter, req *http.Request) {
	err := CheckSecret(req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusUnauthorized)
		return
	}
	ytChannelId := req.URL.Query().Get("channel_id")
	ytChannelName := req.URL.Query().Get("channel_name")

	log.Printf("call to AddChannelToBacklog, ch_id = %v, ch_name = %v", ytChannelId, ytChannelName)
	if ytChannelName == "" && ytChannelId == "" {
		http.Error(w, "no param to find channel by", http.StatusBadRequest)
		return
	}

	ctx := context.Background()
	service, err := youtube.NewService(ctx, option.WithAPIKey(processing.DeveloperKey))
	if err != nil {
		http.Error(w, "error creating new YouTube client", http.StatusBadRequest)
		return
	}

	var backlogItem model.StatisticsBacklog

	var filterParam string
	sqlString := "SELECT channel_id, channel_name FROM statistics_backlog WHERE "
	if ytChannelId != "" {
		sqlString = sqlString + "channel_id = $1"
		filterParam = ytChannelId
	} else {
		sqlString = sqlString + "channel_name = $1"
		filterParam = ytChannelName
	}

	searchErr := DbConn.QueryRow(sqlString, filterParam).Scan(&backlogItem.ChannelId, &backlogItem.ChannelName)
	if searchErr != nil && searchErr != pgx.ErrNoRows {
		http.Error(w, searchErr.Error(), http.StatusBadRequest)
		return
	}

	if searchErr == pgx.ErrNoRows {
		log.Printf("creating new backlog record")
		call := service.Channels.List([]string{"snippet", "contentDetails", "statistics"})
		if ytChannelId == "" {
			call = call.ForUsername(ytChannelName)
		} else {
			call = call.Id(ytChannelId)
		}
		response, err := call.Do()
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}

		ytChannelId = response.Items[0].Id
		ytChannelName = response.Items[0].BrandingSettings.Channel.Title

		backlogItem = model.StatisticsBacklog{
			ChannelId:     ytChannelId,
			ChannelName:   ytChannelName,
			LastRequested: time.Now(),
		}

		sqlString = "INSERT INTO statistics_backlog(channel_id, channel_name, last_requested) " +
			"VALUES($1, $2, $3)"
		_, err = DbConn.Exec(sqlString, backlogItem.ChannelId, backlogItem.ChannelName, backlogItem.LastRequested.String())
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	} else {
		log.Printf("updating existing backlog record")
		backlogItem.LastRequested = time.Now()
		sqlString = "UPDATE statistics_backlog SET last_requested = $1 WHERE channel_id = $2"
		_, err := DbConn.Exec(sqlString, backlogItem.LastRequested.String(), backlogItem.ChannelId)
		if err != nil {
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
	}

	log.Printf("call to AddChannelToBacklog finished without errors")
	writeJson(w, backlogItem)
}
