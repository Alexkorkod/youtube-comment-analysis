package api

import (
	"encoding/json"
	"errors"
	"log"
	"net/http"
	"os"
	"strings"
)

func CheckSecret(r *http.Request) (err error) {
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		return errors.New("no auth token")
	}

	explodedHeader := strings.Split(authHeader, " ")
	if len(explodedHeader) != 2 {
		return errors.New("wrong auth token format")
	}

	if explodedHeader[1] != os.Getenv("auth_key") {
		return errors.New("wrong auth token")
	}

	return
}

func writeJson(w http.ResponseWriter, result interface{}) {
	w.Header().Set("Content-Type", "application/json")

	jsonData, err := json.Marshal(result)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
	} else {
		_, err = w.Write(jsonData)
		if err != nil {
			log.Println(err.Error())
		}
	}
}
