module gitlab.com/alexkorkod/youtube-comment-analysis

go 1.15

require (
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/gofrs/uuid v4.0.0+incompatible // indirect
	github.com/golang/protobuf v1.4.3
	github.com/jackc/fake v0.0.0-20150926172116-812a484cc733 // indirect
	github.com/jackc/pgx v3.6.2+incompatible
	github.com/joho/godotenv v1.3.0
	github.com/lib/pq v1.9.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/shopspring/decimal v1.2.0 // indirect
	google.golang.org/api v0.40.0
	google.golang.org/grpc v1.34.0
	google.golang.org/protobuf v1.25.0
)
