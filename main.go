package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/alexkorkod/youtube-comment-analysis/api"
	"gitlab.com/alexkorkod/youtube-comment-analysis/processing"

	"github.com/jackc/pgx"
	"github.com/joho/godotenv"
)

func initRouting() {
	http.HandleFunc("/statistics", api.GetStatsForChannel)
	http.HandleFunc("/calculate", api.CalculateStatsForChannel)
	http.HandleFunc("/rate_channel", api.RateChannel)
	http.HandleFunc("/add_to_backlog", api.AddChannelToBacklog)
}

func main() {
	err := godotenv.Load(".env")

	if err != nil {
		log.Fatalln("Error loading .env file")
	}
	log.Println(".env loaded successfully")

	config := pgx.ConnConfig{
		Host:     os.Getenv("db_host"),
		Port:     5432,
		Database: os.Getenv("db_name"),
		User:     os.Getenv("db_user"),
		Password: os.Getenv("db_pass"),
	}
	conn, err := pgx.Connect(config)
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "Unable to connect to database: %v\n", err)
		os.Exit(1)
	}
	log.Println("connected to db successfully")

	api.DbConn = conn
	processing.DeveloperKey = os.Getenv("youtube_api_key")
	processing.DbConn = conn
	defer conn.Close()

	initRouting()
	err = http.ListenAndServe(":8080", nil)
	if err != nil {
		log.Fatal(err)
		return
	}
}
